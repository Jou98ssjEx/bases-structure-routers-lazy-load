import { FC, useReducer, ReactNode, useEffect } from 'react';
import { AuthContext, authReducer } from './';


export interface AuthState {
   auth: boolean;
}

const Auth_INITIAL_STATE: AuthState = {
   auth: false,
}

interface Props  {
   children?: ReactNode;
}

const local = () => {
   const a = localStorage.getItem('auth')
   // !a ? Auth_INITIAL_STATE : (JSON.parse(a));
   if(!a)
      return Auth_INITIAL_STATE;
      
   return JSON.parse(a);

}

export const AuthProvider: FC <Props> = ( {children} ) => {

  const init = local(); 
  const [state, dispatch] = useReducer(authReducer, init );
//   const [state, dispatch] = useReducer(authReducer, Auth_INITIAL_STATE );

  // Save in localStorage
  useEffect(() => {
   localStorage.setItem('auth', JSON.stringify(state));
  }, [state]);
  

  const singIn = ( ) => {
   dispatch({
      type:'[Auth] - SingIn',
   })
  }

  const logout = ( ) => {
   dispatch({
      type:'[Auth] - Logout',
   })
  }

  return(
     <AuthContext.Provider value={{
         ...state,

         // methods
         singIn,
         logout,
      }}>
        { children }
     </AuthContext.Provider>
  )

}