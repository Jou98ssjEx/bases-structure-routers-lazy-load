import { useContext } from "react";
import { NavLink, useLocation, useNavigate } from "react-router-dom"
import { AuthContext } from "../../context";
import { linkNav } from "../../utils"

const { publicLink, adminLink } = linkNav;

export const Navbar = () => {

    const { auth } = useContext(AuthContext);

    const navigate = useNavigate();
    const {pathname} = useLocation();

    const handleNavigate = ( url: string) => {
        navigate(url)
    }

  return (
        <nav className="navbar"> 

            <h1>Navigation</h1>

            <div className="navigation">

                {
                    publicLink.map( d => (
                        <div 
                            className="links"
                            key={d.path}
                        >
                            <div 
                                className={ pathname === d.path ? 'item-active': 'item'}
                                // className="item"
                                onClick={() =>handleNavigate(d.path)}
                            >
                                    <span>
                                        {d.name}
                                    </span>
                            </div>
                            
                        </div>
                    ))
                }

                {
                    auth && adminLink.map( d => (
                        <div 
                            className="links"
                            key={d.path}
                        >
                            <div 
                                className={ pathname === d.path ? 'item-active': 'item'}
                                onClick={() =>handleNavigate(d.path)}
                            >
                                    <span>
                                        {d.name}
                                    </span>
                            </div>

                        </div>
                    ))
                }
            </div>
        </nav>
  )
}
