import {useContext} from 'react';
import { AuthContext } from '../../context';

export const Header = () => {

  const { auth, logout } = useContext(AuthContext)

  return (
    <header className='d-flex justify-content-between mx-4 mt-2'>

      Iconos
      {
        auth && (
          <div className="logout">
            <button
              className="btn btn-outline-danger"
              onClick={logout}
            >
              Logout
            </button>
          </div>
        )
      }

         

    </header>
  )
}
