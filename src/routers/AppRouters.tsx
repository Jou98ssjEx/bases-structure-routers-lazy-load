import { Suspense, useContext } from "react";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom"
import { Header, Navbar } from "../components"
import { AuthContext } from "../context";
import { About, AuthPage, Contact, Home, AdminPage, UserPage } from "../pages"
import { linkNav } from "../utils"

const { publicLink, adminLink } = linkNav;
const [ admin, user ] = adminLink;
const [ home, about, contact, auth ] = publicLink;

export const AppRouters = () => {

  const { auth: logged } = useContext(AuthContext);
  return (
    <Suspense
      fallback={ <span> Loading... </span> }
    >
      <BrowserRouter>
        <Header />
        <div className="main-layout">
          <Navbar />

            <Routes>

              {
                publicLink.map( data => (
                  <Route
                    key={data.path}
                    path={data.path}
                    element={ < data.Component title={data.title} />  }
                  />
                ))
              }

                {/* <Route path={home.path} element={ <Home title={home.title} /> } />
                <Route path={about.path} element={<About title={about.title} />} />
                <Route path={contact.path} element={<Contact title={contact.title} /> } />
                <Route path={auth.path} element={<AuthPage title={auth.title} /> } /> */}

                {
                  logged && (
                      <>
                        <Route path={admin.path} element={<AdminPage  /> } />
                        <Route path={user.path} element={<UserPage  /> } />
                      </>
                    )
                  }



                <Route path="/*" element={ <Navigate to='/' replace /> } />
            </Routes>
        </div>
      </BrowserRouter>
    </Suspense>
  )
}
