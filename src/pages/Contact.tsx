import { FC } from "react"
import { IPropsPages } from "../interfaces"

export const Contact:FC<IPropsPages> = ({title}) => {
    document.title = title

  return (
    
    <>
        <h1>
          {title}
        </h1>
    </>
  )
}
