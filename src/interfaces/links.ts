import { FC } from 'react';
import { IPropsPages } from './pages';


export interface Ilinks {
    adminLink: IAdminLinks[];
    publicLink: IPublicLinks[];
}

export interface IAdminLinks {
    name: string,
    title: string,
    path: string,
}
export interface IPublicLinks {
    name: string,
    title: string,
    path: string,
    Component: FC<IPropsPages>
}
// export interface IPublicLinks {
//     name: string,
//     title: string,
//     path: string,
//     Component: FC<IPropsPages>
// }