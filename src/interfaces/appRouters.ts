import { ReactNode } from 'react';


export interface IAppRouter {
    children: ReactNode
}