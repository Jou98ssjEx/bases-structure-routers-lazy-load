import { lazy } from "react";
import { Ilinks } from "../interfaces";
import { Home } from "../pages";


const Lazy1 = lazy( ()=> import('../pages/Home'));


export const linkNav: Ilinks = {
    publicLink:[
        {
            name: 'Home',
            title: 'Home Page',
            path: '/',
            Component: Lazy1
        },
        {
            name: 'About',
            title: 'About Page',
            path: '/about',
            Component: Home

        },
        {
            name: 'Auth',
            title: 'Auth Page',
            path: '/auth',
            Component: Home
        },
    ],

    adminLink: [
        {
            name: 'Admin',
            title: 'Admin Page',
            path: '/admin',
        },
        {
            name: 'Admin User',
            title: 'Admin User Page',
            path: '/users',
        },
        // {
        //     name: 'Logout',
        //     title: 'Logout Page',
        //     url: '/logout',
        // },
    ]
}